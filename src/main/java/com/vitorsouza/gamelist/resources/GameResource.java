package com.vitorsouza.gamelist.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitorsouza.gamelist.models.Game;
import com.vitorsouza.gamelist.services.GameService;

@RestController
@RequestMapping(value="/games")
public class GameResource {
	
	@Autowired
	private GameService gameService;
	
	@GetMapping
	public ResponseEntity<List<Game>> findAllGames(){
		return ResponseEntity.ok(gameService.findAllGames());
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Game> findGameById(@PathVariable Integer id){
		return ResponseEntity.ok(gameService.findGameById(id));
	}
	
	@PostMapping
	public ResponseEntity<Game> insertGame(@Valid @RequestBody Game game){
		return ResponseEntity.ok(gameService.insertGame(game));
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<Game> updateGame(@Valid @RequestBody Game game, @PathVariable Integer id){
		Game oldGame = game;
		oldGame.setId(id);
		return ResponseEntity.ok(gameService.updateGame(oldGame));
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteGame(@PathVariable Integer id){
		gameService.deleteGame(id);
		return ResponseEntity.noContent().build();
	}
	
}
