package com.vitorsouza.gamelist.services;

import java.util.List;
import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitorsouza.gamelist.dao.GameDAO;
import com.vitorsouza.gamelist.models.Game;

@Service
public class GameService {
	
	@Autowired
	private GameDAO gameDAO;
	
	public List<Game> findAllGames(){
		return gameDAO.findAll();
	}
	
	public Game insertGame(Game game){
		return gameDAO.save(game);
	}
	
	public Game findGameById(Integer id){
		Optional<Game> g = gameDAO.findById(id);
		return g.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Game.class.getName(), null));
	}
	
	public void deleteGame(Integer id){
		Game game = findGameById(id);
		gameDAO.delete(game);
	}
	
	public Game updateGame(Game oldGame){
		Game newGame = findGameById(oldGame.getId());
		this.updateData(newGame, oldGame);
		return gameDAO.save(newGame);
	}
	
	private void updateData(Game newGame, Game oldGame) {
		newGame.setName(oldGame.getNome());
		newGame.setDescricao(oldGame.getDescricao());
		newGame.setNota(oldGame.getNota());
	}
	
}
