package com.vitorsouza.gamelist.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vitorsouza.gamelist.models.Game;

@Repository
public interface GameDAO extends JpaRepository<Game, Integer>{

}
