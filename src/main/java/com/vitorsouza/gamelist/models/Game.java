package com.vitorsouza.gamelist.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Game {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String nome;
	
//	@OneToMany(fetch = FetchType.LAZY)
//	private List<String> plataformas = new ArrayList<String>();

	private String descricao;
	
	private int nota;
	
	private String url;
	
	private boolean jogado;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setName(String nome) {
		this.nome = nome;
	}
//	public List<String> getPlataformas() {
//		return plataformas;
//	}
//	public void setPlataformas(List<String> plataformas) {
//		this.plataformas = plataformas;
//	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getNota() {
		return nota;
	}
	public void setNota(int nota) {
		this.nota = nota;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isJogado() {
		return jogado;
	}
	public void setJogado(boolean jogado) {
		this.jogado = jogado;
	}
	
	
}
